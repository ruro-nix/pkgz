z:
let
  inherit (z.inputs) libz upstream-nixpkgs;

  # Auto populate exports from folders
  exports.auto = [./.];

  # Describes the desired modifications for nixpkgs
  nixpkgsOptions = {
    config = import ./config.nix z;
    overlays = libz.flakes.getOverlays "pkgOverlays" z.inputs;
  };

  # Apply these changes to nixpkgs themselves.
  forAllSystems = libz.genAttrs libz.systems.flakeExposed;
  mkPkgs = system: import upstream-nixpkgs (nixpkgsOptions // { inherit system; });
  legacyPackages = forAllSystems mkPkgs;
in
{
  inherit legacyPackages nixpkgsOptions exports;

  # We currently don't export the following upstream flake outputs:
  #   lib, nixosModules, htmlDocs, checks
}
