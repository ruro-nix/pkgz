{
  description = "Wrapper around the official nixpkgs with some modifications";
  outputs = { z, ... }: import ./pkgz z;
}
